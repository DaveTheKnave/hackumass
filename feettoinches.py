#!/bin/python3
last = input("Start putting in numbers in format 'feet.inches'. Each should be separated by a new line. When done, press enter twice.\n\n")
lines = []
while last is not "":
    lines.append(last)
    last = input()

print("# OUTPUT #\n")

for i in lines:
    first = ""
    second = ""
    after = False
    for j in i:
        if j is '.':
            after = True
        elif not after:
            first = first + j
        else:
            second = second + j
    if not after:
        inches = int(first) * 12
    else:
        inches = int(first) * 12 + int(second)
    print(inches)

