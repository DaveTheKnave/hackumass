package tailorswift.survivalofthefitted;




import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;

public class ResultsActivity extends AppCompatActivity {

    ListView entries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        String[] results = intent.getStringArrayExtra("Parameters");
        setContentView(R.layout.activity_results);
        entries = (ListView) findViewById(R.id.entries);
        populateList(results);
    }
   // ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, android.R.id.text1, results);

    private void populateList(String[] results){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, android.R.id.text1, results);
        entries.setAdapter(adapter);
    }
}
