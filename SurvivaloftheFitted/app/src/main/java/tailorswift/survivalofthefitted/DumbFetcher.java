package tailorswift.survivalofthefitted;

public class DumbFetcher extends Fetcher{
    public String[] getClothes(String brand, String size){
        String input = brand.toUpperCase();
        int sizeNumber;
        try {
            sizeNumber = Integer.valueOf(size);
        }
        catch (NumberFormatException e){
            sizeNumber = -9000;
        }
        if (input.equals("GAP")){
            if (sizeNumber == 8){
                return new String[]{"P&L 30", "Lucky Brand 10/30"};
            }
            if (sizeNumber == 6){
                return new String[]{"Altar'd State 7","Limited 4"};
            }
        }
        if (input.equals("OLD NAVY")){
            if (sizeNumber == 14){
                return new String[]{"Banana Republic 29","Michael Kors 6"};
            }
            if (sizeNumber == -1){
                return new String[]{"American Eagle 6","Tuff Athletics -2"};
            }
        }
        return new String[]{"No Results"};
    }

    public String[] getBrands(){
        return new String[]{"Old Navy", "Altar'd State", "Banana Republic", "American Eagle", "Michael Kors", "Tuff Athletics"};
    }
}
