package tailorswift.survivalofthefitted;

public abstract class Fetcher {
    public abstract String[] getClothes(String brand, String size);

    public abstract String[] getBrands();
}
