package tailorswift.survivalofthefitted;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    String brand;
    String size;
    EditText brands;
    EditText sizes;
    Button go;
    Fetcher fetcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fetcher = new SQLiteFetcher(this);
        brands = (EditText) findViewById(R.id.editText);
        sizes = (EditText) findViewById(R.id.sizeText);
        go = (Button) findViewById(R.id.button);
        go.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                execute();
            }
        });

    }

    protected void execute() {
        brand = brands.getText().toString();
        size = sizes.getText().toString();
        String[] results = fetcher.getClothes(brand,size);
        System.out.println("Results:");
        for(String s: results){
            System.out.println(s);
        }
        showResultsScreen(results);
    }

    protected void showResultsScreen(String[] string){
        Intent myIntent = new Intent(this, ResultsActivity.class);
        myIntent.putExtra("Parameters", string); //Optional parameters
        startActivity(myIntent);
    }


}
